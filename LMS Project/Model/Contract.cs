﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static LMS_Project.GlobalVar;

namespace LMS_Project
{
    class Contract : IService
    {
        private int _serviceNum;
        private Type_Service _serviceType;
        private Type_Package _packageType;
        private Client _sender;
        private Client _requester;
        private string _receptionAddress;
        private string _deliveryAddress;

        private int _contractPeriod;

        public Contract(int serviceNum, Type_Service serviceType, Type_Package packageType, Client sender, Client requester, string receptionAddress, string deliveryAddress, int contractPeriod)
        {
            _serviceNum = serviceNum;
            _serviceType = serviceType;
            _packageType = packageType;
            _sender = sender;
            _requester = requester;
            _receptionAddress = receptionAddress;
            _deliveryAddress = deliveryAddress;
            _contractPeriod = contractPeriod;
        }

        public int ServiceNum { get { return _serviceNum; } set { _serviceNum = value; } }
        public Type_Service ServiceType { get { return _serviceType; } set { _serviceType = value; } }
        public Type_Package PackageType { get { return _packageType; } set { _packageType = value; } }
        public Client Sender { get { return _sender; } set { _sender = value; } }
        public Client Requester { get { return _requester; } set { _requester = value; } }
        public string ReceptionAddress { get { return _receptionAddress; } set { _receptionAddress = value; } }
        public string DeliveryAddress { get { return _deliveryAddress; } set { _deliveryAddress = value; } }
        public int ContractPeriod { get { return _contractPeriod; } set { _contractPeriod = value; } }
    }
}
