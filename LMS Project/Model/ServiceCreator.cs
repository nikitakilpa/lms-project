﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static LMS_Project.GlobalVar;

namespace LMS_Project
{
    abstract class ServiceCreator : NotifyPropertyChanged
    {
        public abstract RelayCommand CreateService { get; }
    }
}
