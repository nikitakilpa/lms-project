﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static LMS_Project.GlobalVar;

namespace LMS_Project
{
    class OrderCreatorViewModel : ServiceCreator
    {
        Order NewOrder;

        #region ORDER_PROPORTIES
        private int _serviceNum;
        private Type_Service _serviceType;
        private Type_Package _packageType;
        private Client _sender;
        private Client _requester;
        private string _receptionAddress;
        private string _deliveryAddress;

        public int ServiceNum { get { return _serviceNum; } set { _serviceNum = value; OnPropertyChanged("ServiceNum"); } }
        public Type_Service ServiceType { get { return _serviceType; } set { _serviceType = value; OnPropertyChanged("ServiceType"); } }
        public Type_Package PackageType { get { return _packageType; } set { _packageType = value; OnPropertyChanged("PackageType"); } }
        public Client Sender { get { return _sender; } set { _sender = value; OnPropertyChanged("Sender"); } }
        public Client Requester { get { return _requester; } set { _requester = value; OnPropertyChanged("Requester"); } }
        public string ReceptionAddress { get { return _receptionAddress; } set { _receptionAddress = value; OnPropertyChanged("ReceptionAddress"); } }
        public string DeliveryAddress { get { return _deliveryAddress; } set { _deliveryAddress = value; OnPropertyChanged("DeliveryAddress"); } }
        #endregion

        public OrderCreatorViewModel()
        {

        }

        private RelayCommand createService;
        public override RelayCommand CreateService
        {
            get
            {
                return createService ??
                    (createService = new RelayCommand(obj =>
                    {
                        NewOrder = new Order(_serviceNum, _serviceType, _packageType, _sender, _requester, _receptionAddress, _deliveryAddress);
                    }));
            }
        }
    }
}
