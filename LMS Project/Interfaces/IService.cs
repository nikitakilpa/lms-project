﻿using static LMS_Project.GlobalVar;

namespace LMS_Project
{
    interface IService
    {
        int ServiceNum { get; set; }
        Type_Service ServiceType { get; set; }
        Type_Package PackageType { get; set; }
        Client Sender { get; set; }
        Client Requester { get; set; }
        string ReceptionAddress { get; set; }
        string DeliveryAddress { get; set; }
    }
}
